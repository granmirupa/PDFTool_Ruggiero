package pdftool;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.rendering.PDFRenderer;

public class PDF {
	
	PDDocument pdfFile = null;
	File file = null;
	
	public PDF(String path){
		try{
			file = new File(path);
			pdfFile = PDDocument.load(file);
		}
		catch(Exception e){
			System.out.println("Error: file not valid");
		}
	}
	
	public int getNumPages(){
		return pdfFile == null ? -1 : pdfFile.getNumberOfPages();
	}
	
	public int removePage(int nPage){
		if(nPage < 0 || nPage >= getNumPages())
			return getNumPages();
		pdfFile.removePage(nPage);
		return getNumPages();
	}
	
	public PDPageTree getPages(){		
		return pdfFile == null ? new PDPageTree() : pdfFile.getPages();
	}
	
	public PDFRenderer getRender(){
		return new PDFRenderer(pdfFile);
	}
	
	public boolean save(String path){
		try {
			pdfFile.save(path);
		} catch (IOException e) {
			System.out.println("Error saving file");
			return false;
		}
		return true;
	}
	
	public boolean save(String path, LinkedList<Integer> toRemove){
		//List <Integer> list = Arrays.asList(toRemove.toArray());
		LinkedList<Integer> toRemove2 = new LinkedList<Integer>(toRemove);
		Collections.sort(toRemove);
		if(!path.endsWith(".pdf")){
			path = path + ".pdf";
		}
			System.out.println(toRemove.toString());
		while(!toRemove2.isEmpty()){
			removePage(toRemove2.pop());
		}
		boolean result = save(path);
		try {
			pdfFile = PDDocument.load(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public static void merge(File[] selectedFiles, File saveFile) {
		try {
			System.out.println("Ciao");
			String pathS = saveFile.getAbsolutePath().endsWith(".pdf") ? saveFile.getAbsolutePath() :  (saveFile.getAbsolutePath() + ".pdf"); 
			PDFMergerUtility PDFmerger = new PDFMergerUtility(); 
			PDFmerger.setDestinationFileName(pathS);
			for(int i = 0; i < selectedFiles.length; i++){
				System.out.println(selectedFiles[i]);
				PDDocument document = PDDocument.load(selectedFiles[i]);
				PDFmerger.addSource(selectedFiles[i]);
				
			}
			if(selectedFiles.length > 0){
				PDFmerger.mergeDocuments(null);
				Object[] options = {"Merge PDFs",
		        "Modify PDF"};
				JFrame f = new JFrame("PDFTool - Ruggiero");
				JOptionPane.showMessageDialog(f, "Saved");
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Eccezione: " + e);
		}
		finally{
			System.exit(0);
		}
	}
}
