package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import javax.jws.Oneway;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
















import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.rendering.PDFRenderer;

import pdftool.PDF;

public class GUI extends JFrame{
	static PDF file = null;
	static List <BufferedImage> images = null;
	static PDFRenderer render;
	
	private List<JButton> jButtons;
	private LinkedList<Integer> toRemove;
	private HashMap<JButton, Integer> hash;
	private GUI gui;
	private GUI(){
		setTitle("PDFTool - Ruggiero");
		gui = this;
		JPanel container = new JPanel();
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		container.setAlignmentX(CENTER_ALIGNMENT);
		JScrollPane scrPane = new JScrollPane(container);
		scrPane.getVerticalScrollBar().setUnitIncrement(20);
		scrPane.setAlignmentX(CENTER_ALIGNMENT);
		this.getContentPane().add(scrPane);
		toRemove = new LinkedList<Integer>();
		
		hash = new HashMap<JButton, Integer>(images.size());
		
		
		createMenuBar();
		jButtons = new ArrayList<JButton>(file.getNumPages());
		
		for(int i = 0; i < images.size(); i++){
			BufferedImage im = images.get(i);
			JButton button = new JButton(new ImageIcon(im));
			button.setText(new Integer(i+1).toString());
			hash.put(button, i);
			button.setAlignmentX(CENTER_ALIGNMENT);
			button.setVerticalTextPosition(SwingConstants.TOP);
			button.setFont(new Font(button.getFont().getFontName(),Font.PLAIN,40));
			button.setFocusPainted(false);
			button.addActionListener(new ActionListener() {
			
				@Override
				public void actionPerformed(ActionEvent e) {
					button.setVisible(false);
					if(hash.get(button) != null)
						System.out.println(hash.get(button));
					toRemove.add(hash.get(button));
				}
			});
			jButtons.add(button);
			container.add(button);
		}
		scrPane.addMouseListener(new MouseListener() {
			
		
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Pussy");
				if(SwingUtilities.isRightMouseButton(e)){
					//System.out.println(toRemove.isEmpty());
					if(!toRemove.isEmpty()){
						jButtons.get(toRemove.pollLast()).setVisible(true);
					}   
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				
				
			}
		});
		setSize(new Dimension(images.get(0).getWidth()+200, 1024));
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void createMenuBar() {

        JMenuBar menubar = new JMenuBar();
        ImageIcon icon = new ImageIcon("exit.png");
        
        JMenuBar menubar2 = new JMenuBar();
        
        
        
        JMenu file2 = new JMenu("File");
        file2.setMnemonic(KeyEvent.VK_F);

        JMenuItem eMenuItem = new JMenuItem("Exit", icon);
        eMenuItem.setMnemonic(KeyEvent.VK_E);
        eMenuItem.setToolTipText("Exit application");
        eMenuItem.addActionListener((ActionEvent event) -> {
            System.exit(0);
        });
        
        JMenuItem eMenuItem2 = new JMenuItem("Save");
        eMenuItem2.setMnemonic(KeyEvent.VK_S);
        eMenuItem2.setToolTipText("Save document");
        eMenuItem2.addActionListener((ActionEvent event)->{
        	 JFileChooser saveFile = new JFileChooser();
        	 FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF Files", "pdf");
        	 saveFile.setFileFilter(filter);
             saveFile.showSaveDialog(null);
             boolean result = file.save(saveFile.getSelectedFile().getAbsolutePath(),toRemove);
             if(!result)
     			JOptionPane.showMessageDialog(gui, "Error saving the file");
             else
            	 JOptionPane.showMessageDialog(gui, "Saved!");
        });
        
        JMenuItem eMenuItem3 = new JMenuItem("Open");
        eMenuItem3.setMnemonic(KeyEvent.VK_O);
        eMenuItem3.setToolTipText("Open document");
        eMenuItem3.addActionListener((ActionEvent event)->{
        	 JFileChooser saveFile = new JFileChooser();
        	 FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF Files", "pdf");
        	 saveFile.setFileFilter(filter);
             saveFile.showOpenDialog(null);
             singleton(saveFile.getSelectedFile().getAbsolutePath());
             gui.dispose();
        });
        file2.add(eMenuItem3);
        file2.add(eMenuItem2);
        file2.add(eMenuItem);

        menubar.add(file2);

        setJMenuBar(menubar);
    }

	
	public static void main(String [] str){
		
		Object[] options = {"Merge PDFs",
        "Modify PDF"};
		JFrame f = new JFrame("PDFTool - Ruggiero");
		int n = JOptionPane.showOptionDialog(f,"What do you want to do?",
			"Starter choose",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null,     //do not use a custom Icon
			options,  //the titles of buttons
			options[0]); //default button title
					
	if(n == 1){
	   	 JFileChooser saveFile = new JFileChooser();
	   	 FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF Files", "pdf");
	   	 saveFile.setFileFilter(filter);
	     saveFile.showOpenDialog(null);
     try{
    	 singleton(saveFile.getSelectedFile().getAbsolutePath());
     }
     
     catch(Exception e){
    	 e.printStackTrace();
    	 System.exit(0);}
	
	}
	else if(n == 0){
	   	 JFileChooser saveFile = new JFileChooser();
	   	 FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF Files", "pdf");
	   	 saveFile.setFileFilter(filter);
	     saveFile.setMultiSelectionEnabled(true);
	   	 saveFile.showOpenDialog(null);
	   	 
	   	 JFileChooser saveFile2 = new JFileChooser();
	   	// FileNameExtensionFilter filter = new FileNameExtensionFilter("PDF Files", "pdf");
	   	 saveFile2.setFileFilter(filter);
	     saveFile2.showSaveDialog(null);
	   	 
	   	 PDF.merge(saveFile.getSelectedFiles(),saveFile2.getSelectedFile());
	}
	else
		System.exit(0);
		//singleton("rugsa283_TDDB84_UPG1_HT2016.pdf");
		
	}
	public static void singleton(String path){
		
		file = new PDF(path);
		//file.removePage(1);
		//file.save("newPDF.pdf");
		//int pages = file.getNumPages();
	//	if(images == null)
			images = new ArrayList<BufferedImage>(file.getNumPages());
		render = file.getRender();
		for(int i = 0; i < file.getNumPages(); i++){
			try {
				images.add(render.renderImage(i));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		new GUI();
	}
}
